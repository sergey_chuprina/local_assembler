'use strict';

var linksReplace = [
{
	old: '<?=$base_url ?>css/signup<?=$flow_var?>/main.php?cache_control=<?=Ihub_Config::get()->retrieve("main", "css_cache_control"); ?>',
	new: 'css/app.css',
},
{
	old: '<?=$image_url?>',
	new: 'images/'
},
{
	old: '<?=$base_url_img?>',
	new: 'images'
},
{
	old: '<?=$logo?>',
	new: 'images/logo.png'
},
{
	old: "<?=$_SESSION['user_kw']?>",
	new: ''
},
{
	old: '<?=$site_name?>',
	new: 'Site name'
},
{
	old: '<?= SignupController::$robots_tag ?>',
	new: 'NOINDEX, NOFOLLOW, NOARCHIVE, NOODP, NOIMAGEINDEX, NOSNIPPET'
},
{
	old: '<?=$base_url?>js/signup<?=$flow_var?>/modernizr.js',
	new: 'js/modernizr.js'
},
{
	old: "<?=strtolower($_SESSION['lng'])?>",
	new: 'en'
}
];

module.exports.links = linksReplace