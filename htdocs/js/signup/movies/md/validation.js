

$(function() {
    var brwsIe7 = $('.ie7');
    var preload = new Image();
    //preload.src = "<?php echo Ihub_Config::get()->retrieve('main', 'secure_base_url').'images/signup/eight/loading.gif'?>";
    var $overlay = $("<div class=\'overlay\'><p><?=getWord('W01314')?></p></div>").css({'display': 'none'});
    $('body').append($overlay);

    var inpt = $('input, select'),
    timer = null,
    timedout = 200,
    isAvs = $('.avs'),
    isReg = $('.reg'),
    isCC = $('.cc');

    function overlayOn(){
        $('.overlay').show();
    }

    //////////////////////////////////////
    ///    AVS arrays for autofill    ////
    //////////////////////////////////////

    var states = new Array('Select State', 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District Of Columbia', 'Florida', 'Georgia', 'Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming');
    var states_abb = new Array('', 'AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');
    var provinces = new Array('Select Province', 'Alberta', 'British Columbia', 'Manitoba', 'New Brunswick', 'Newfoundland', 'Nova Scotia','Nunavut','North West Territories','Ontario','Prince Edward Island','Quebec','Saskatchewen','Yukon');
    var prov_abb = new Array('','AB','BC','MB','NB','NL','NT','NS','NU','ON','PE','QC','SK','YT');
    var dd_country = $('#fake_country');

    /* Builds dropdown for US states and Canada provinces */
    if(isAvs.length){
        dd_country.on('change', setautocomplete);
        setautocomplete();
    }


    /* Signup Form validation (step 1 & step 2) */
    $('#signup').submit(function(){
        $('p.msgs').remove();
        var dForm = $(this);
        var canSubmit = true;
        var emptyField = false;
        var inputField = dForm.find('input, select, input[type=radio], input[type=checkbox]');

        if(inputField.attr('name') == 'zip_code' || inputField.attr('name') == 'ccn'){
            var self = $(this);
            convertString(self);
        }
        inputField.each(function(){

            //$(this).css({"background":"red"});
            if($(this).attr('type') !== 'submit' && $(this).attr('type') !== 'button' && $(this).attr('type') !== 'hidden')
                //&& $(this).attr('id') !== 'fake_country')
            {
                // console.log($(this));
                if(!verifyFields(this, 'check')){
                    //console.log(this);
                    //console.log("invalid");
                    canSubmit = false;
                    if(
                        $(this).attr('type') == 'text' && !$(this).val()
                        || $(this).attr('class') == 'input__checkbox' && !$(this).val()
                        || $(this).attr('class') == 'input__radio' && !$(this).val()
                        || $(this)[0].nodeName.toLowerCase() == "select"
                        && $(this).val() == 0) {
                            //console.log(this);
                            emptyField = true;
                    }
                }
            }
        });

        if(emptyField){
            inputField.each(function(){
                // console.log($(this).attr("class"));
                // console.log($(this));
                //$(this).css({"background":"red"});
            });
            $("html, body").animate({ scrollTop: "100" });
            //$(".invalid-empty").first().focus();
            $(".form__error").show();
            dForm.removeClass("update").addClass("empty");
        };

        if(canSubmit) {
            overlayOn();
            disableBtn();
            $(".form__error").hide();
            dForm.removeClass('invalid-form');
            if(isReg.length){
                addCookie();
            }
            return true;
        } else {
            dForm.addClass('invalid-form');
            if(!emptyField && isCC.length){
                dForm.removeClass("empty").addClass("update");
            }
            return false;
        }
        return false;
    });

    /* Attaching validation to all events */
    inpt.on('blur', function(evnt){

        if($(this).attr('type') !== 'submit' && $(this).attr('type') !== 'button' && $(this).attr('type') !== 'hidden'){

            if(evnt.type == 'blur' && code !== 9){
                clearTimeout(timer);
            }

            var code = (evnt.keyCode ? evnt.keyCode : evnt.which),
                _this = this,
                element = $(this),
                namePattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,
                emailIn = element.attr('id') == 'username',
                passIn = element.attr('id') == 'password',
                inVal = element.val(),
                zipC = element.attr('id') == 'zip_code',
                cvvIn = element.attr('id') == 'cvv2_code',
                ccTest = /\D/g,
                zipTest = /\s/g,
                elCcn = element.attr('id') == 'ccn',
                // Custom
                // prodDiaper = element.attr('id') == 'checkbox-diapers',
                // prodProducts = element.attr('id') == 'checkbox-products',
                // prodSize = element.attr('id') == 'select-size',
                // sexBoy = element.attr('id') == 'sex-boy',
                // sexGirl = element.attr('id') == 'sex-girl';

            //set timer for setTimeout, extra time for ccn field
            timedout = elCcn ? 500 : 200;
            if(evnt.type == 'focus' && element.prev('p.msgs')){
                $('p.msgs').hide();
                element.prev('p.msgs').fadeIn();
            }
            if(evnt.type !== 'blur' ) {
                var evt = evnt;
                if(emailIn){
                    if(namePattern.test(inVal)){
                        timer = setTimeout(function(){
                            verifyFields(_this,''+evt);
                        }, timedout);
                    }
                } else if(passIn) {
                    if(inVal.length >= 6){
                        timer = setTimeout(function(){
                            verifyFields(_this,''+evt);
                        }, timedout);
                    }
                } else if(elCcn) {
                    if(inVal.length >= 16){
                        timer = setTimeout(function(){
                            if(ccTest.test(inVal)){
                               convertString(element);
                            }
                            verifyFields(_this,''+evnt);
                        }, timedout);
                    }
                } else if(zipC) {
                    convertString(element);
                    if(inVal.length >= 3) {
                        timer = setTimeout(function(){
                            verifyFields(_this,''+evt);
                        }, timedout);
                    }
                } else if(cvvIn) {
                    if(inVal.length >= 3) {
                        timer = setTimeout(function(){
                            verifyFields(_this,''+evt);
                        }, timedout);
                    }
                } else if(inVal.length) {
                    timer = setTimeout(function(){
                        verifyFields(_this,''+evnt);
                    }, timedout);
                }
            }

            // if(evnt.type == 'blur'){
            //     alert("blurred empty");
            //     element.prev('p.msgs').fadeOut();
            //     //verifyFields(_this,''+evnt);
            // }
            // if(evnt.type !== 'blur' && evnt.type !== 'keyup'){
            //     element.prev('p.msgs').fadeOut();
            //     if(zipC || elCcn){
            //         convertString(element);
            //         verifyFields(_this,''+evnt);
            //     } else {
            //         //verifyFields(_this,''+evnt);
            //     }
            // }
            // if(evnt.type == 'blur'){
            //     verifyFields(_this,''+evnt);
            // }
            if(evnt.type == 'blur'){
                if(inVal.length){
                    verifyFields(_this,''+evnt);
                }else{
                    clearValidation(_this);
                }
            }

        }
    });



function setautocomplete(){
        var select_ind = document.secureForm.elements["fake_country"].selectedIndex,
            country_el = document.secureForm.elements["fake_country"].options[select_ind].value,
            whole_str="",
            regionCon = $('.region').find(".input-wrap"),
            tabIndex = $('.region').find("[name=state]").attr("tabindex"),
            setState = '<?=(isset($_GET["state"]) ? $_GET["state"] : "")?>';

        if(country_el == "CA" || country_el == "US"){
            if(country_el=="CA"){
                var str = "<?=getWord('W00412')?>";

                for(var i=0; i < provinces.length; i++){
                    var dd_str = "<option value='"+prov_abb[i]+"'";
                    if(setState == prov_abb[i])
                        dd_str += "selected=selected";
                    dd_str += ">" + provinces[i] +"</option>";
                    whole_str =  whole_str+dd_str;
                }
            }
            else{
                var str = "<?=getWord('W00411')?>";
                for(var i=0; i < states.length; i++){
                    var dd_str = "<option value='"+states_abb[i]+"'";
                    if(setState == states_abb[i])
                        dd_str += "selected=selected";
                    dd_str += ">" + states[i] +"</option>";
                    whole_str =  whole_str+dd_str;
                }
            }
            regionCon.find("input").remove();
            regionCon.html("<select id='dd_state' name='state' tabindex='"+tabIndex+"'>"+ whole_str +"</select>");
        }
        else {
            regionCon.find("input").remove();
            regionCon.html("<input type=text name=state id=txt_state value=\"<?php echo isset($_POST['state']) ? $_POST['state'] : '' ?>\" rel=type-2 placeholder=\"<?=getWord('W01087')?>\" tabindex='"+tabIndex+"' />");
        }

        $('input, select').on('blur', function(evnt){

            // On blur of elements

            if($(this).attr('type') !== 'submit' && $(this).attr('type') !== 'button' && $(this).attr('type') !== 'hidden') {
                if(evnt.type == 'blur'){
                    clearTimeout(timer);
                }

                var code = (evnt.keyCode ? evnt.keyCode : evnt.which),
                    _this = this,
                    element = $(this),
                    inVal = element.val(),

                //set timer for setTimeout, extra time for ccn field
                timedout = 200;
                // blur, verify field:
                if(evnt.type == 'blur'){
                    if(inVal.length){
                        verifyFields(_this,''+evnt);
                    }else{
                        clearValidation(_this);

                    }
                }
            }
        });
    }

    //utility function for converting string;
    function convertString(element){
        //ternary sets regX to remove whitspace if element id is zip_code
        //or sets regX to remove all characters that are not numbers for CCN
        //refactor this function if more elements/regEx matching is needed
        var regX = element.attr('id') == 'zip_code' ? /\s/g : /\D/g;
        //remove whitespace, convert to uppercase
        var val = element.attr('id') == 'zip_code' ? element.val().replace(regX, '').toUpperCase() : element.val().replace(regX, '');
        //asign converted value to input
        element.val(val);
    }

    /* Validation per field, using the element ID */
    function verifyFields(element, event) {
        var elt = document.getElementById(element.id);
        // console.log(elt);
        switch (elt.id) {
            case 'username':
                if(elt.value != '') {
                    var namePattern = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                    if (!namePattern.test(elt.value)) {
                        fireMessage("You must provide an email", elt);
                       // fireMessage("Please enter a correct first name.", elt);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        //removeErrorMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                       fireMessage("The email you provided is not valid", elt);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
            case 'password':
                if(elt.value != '') {
                    if (elt.value.length < 6) {
                        elt.value = elt.value.substring(0,6);
                        fireMessage("Your password must be at least 6 characters long", elt);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        //removeErrorMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                       fireMessage("Your password must be at least 6 characters", elt);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
            case 'first-name':
                if(elt.value != '') {
                    var namePattern = /^[a-z ,.'-]+$/i;
                    if (elt.value.length > 30) {
                        elt.value = elt.value.substring(0,30);
                        fireMessage("First name cannot exceed 30 characters.", elt);
                        if (event == 'check')
                            return false;
                    } else if (!namePattern.test(elt.value)) {
                        fireMessage("Please enter your first name", elt);
                       // fireMessage("Please enter a correct first name.", elt);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        //removeErrorMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W00781'))))?>", elt);
                        if (event == 'check')
                            return false;
                    }
                }
                break;
            case 'last-name':
                if(elt.value != '') {
                    var namePattern = /^[a-z ,.'-]+$/i;
                    if (elt.value.length > 30) {
                        elt.value = elt.value.substring(0,30);
                        fireMessage("Last name cannot exceed 30 characters", elt);
                        if (event == 'check')
                            return false;
                    } else if (!namePattern.test(elt.value)) {
                        fireMessage("Please enter your last name", elt);
                        if (event == 'check')
                            return false;
                    } else {
                       removeMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W00782'))))?>", elt);
                        if (event == 'check')
                            return false;
                    }
                }
                break;
            case 'address':
                if(elt.value != '') {
                var str = elt.value.split(" ",2);
                var st_no = str[0];
                var st_name = str[1];
                    if (st_no == '' && st_name == '') {
                        fireMessage("Please enter a valid address", elt);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01085'))))?>", elt);
                        if (event == 'check')
                            return false;
                    }
                }
                break;
            case 'city':
                if(elt.value != '') {
                    if (elt.value.length >= 80) {
                        elt.value = elt.value.substring(0,80);
                        fireMessage("City cannot exceed 80 characters.", elt);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01086'))))?>", elt);
                        if (event == 'check')
                            return false;
                    }
                }
                break;
            case 'dd_state':
            case 'txt_state':
                if(elt.value != '') {
                    if (elt.value.length > 30) {
                        elt.value = elt.value.substring(0,80);
                        fireMessage("State cannot exceed 80 characters.", elt);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'blur') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01087')).'/'.strtolower(getWord('W01088'))))?>", elt);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
            case 'zip_code':
                if(elt.value!=''){
                    var zipPattern = /^[A-Z0-9\-]{3,10}$/i ;
                    if (!zipPattern.test(elt.value)) {
                        fireMessage("Zip/Postal Code must be between 3 and 10 characters.", elt, event);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        if (event == 'check')
                            return true;
                    }
                } else {
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01089'))))?>", elt, event);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
            case 'fake_country':
                if(elt.value!=''){
                    removeMessage(elt);
                    if (event == 'check')
                        return true;
                } else {
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01089'))))?>", elt, event);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
            case 'ccn':
                    if(elt.value != '') {
                        var VisaPattern = /^4[0-9]{12,15}$/;
                        var MasterCardPattern = /^5[0-9]{15}$/;
                        if(VisaPattern.test(elt.value)) {
                            if (!luhn_check(elt.value)) {
                                fireMessage("Invalid VISA card number", elt);
                                //fireMessage("Invalid VISA card number.", elt);
                                if (event == 'check')
                                    return false;
                            } else {
                                 removeMessage(elt);
                                //removeErrorMessage(elt);
                                if (event == 'check')
                                    return true;
                            }
                        } else if (MasterCardPattern.test(elt.value)) {
                            if (!luhn_check(elt.value)) {
                                fireMessage("Invalid MASTERCARD number", elt);
                                //fireMessage("Invalid MASTERCARD number.", elt);
                                if (event == 'check')
                                    return false;
                            } else {
                                 removeMessage(elt);
                                //removeErrorMessage(elt);
                                if (event == 'check')
                                    return true;
                            }
                        } else {
                            fireMessage("Only VISA/MASTERCARD are accepted", elt);
                            //fireMessage("Only VISA/MASTERCARD are accepted.", elt);
                            if (event == 'check')
                                return false;
                        }
                    } else {
                        emptyField("Only VISA/MASTERCARD are accepted", elt);
                        //fireMessage("Please enter your credit card number.", elt);
                        if (event == 'check')
                            return false;
                    }
                break;
            case 'cvv2_code':
                var ccv2Pattern = /^[0-9]{3,4}$/;
                    if (elt.value != ''){
                        if (!ccv2Pattern.test(elt.value)){
                            //fireMessage("<?=getWord('W01120', array('CVV2'))?>", elt);
                            fireMessage("Invalid CCV2 number.", elt,10,210);
                            if (event == 'check')
                                return false;
                        } else {
                            removeMessage(elt);
                            //removeErrorMessage(elt);
                            if (event == 'check')
                                return true;
                        }
                    } else {
                        if (event != 'keyup') {
                            emptyField("<?=getWord('W01318')?>", elt);
                            //fireMessage("Please enter your CCV2 number.", elt,10,210);
                            if (event == 'check')
                                return false;
                        }
                    }
                break;
            case 'month':
                var day_array = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
                var month = elt.value.indexOf('0') == 0 ? elt.value.substring(1) : elt.value;
                month -= 1;
                if (elt.value != 0) {

                    var year = document.getElementById('year').value;
                    if (year != 0) {

                        var day = month == 1 ? (year % 4 == 0 ? 29 : 28) : day_array[month];
                        var expDate = new Date();
                        expDate.setFullYear(year, month, day, 23, 59, 59);
                        var today = new Date();
                        if (today > expDate) {

                             fireMessage("The expiry date is already passed.", document.getElementById('year'));
                            //fireMessage("The expiry date is already passed.", elt,20,282);
                            if (event == 'check')
                                return false
                        } else {
                            //removeErrorMessage(elt);
                            removeMessage(document.getElementById('year'));
                            //removeErrorMessage
                            if (event == 'check')
                                return true;
                        }
                    }
                } else {
                    emptyField("<?=getWord('W01120', array(getWord('W01092')))?>",  document.getElementById('year'));
                    if (event == 'check')
                        return false;
                }
                break;
            case 'year':
                var day_array = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
                var year = elt.value;
                if (year != 0) {
                    var month = document.getElementById('month').value;
                    if (month.indexOf('0') == 0 && month.length > 1) {
                        month = month.substring(1);}

                    month --;

                    var day = month == 1 ? (year % 4 == 0 ? 29 : 28) : day_array[month];
                    var expDate = new Date();
                    expDate.setFullYear(year,month,day,23,59,59);
                    var today = new Date();
                    if (today > expDate) {
                        //fireMessage("<?=getWord('W01123')?>", elt);
                        fireMessage("The expiry date is already passed.", elt, 20,120);
                        if (event == 'check')
                            return false;
                    } else {
                        removeMessage(elt);
                        //removeErrorMessage(elt);
                       // removeErrorMessage(document.getElementById('month'));
                        if (event == 'check')
                            return true
                    }

                } else {
                   emptyField("<?=getWord('W01120', array(strtolower(getWord('W01091'))))?>",elt);
                   // fireMessage("Please, select an expiry date.",elt,20,100);
                    if (event == 'check')
                        return false;
                }
                break;
            // DDC specific
            case 'checkbox-diapers':
            case 'checkbox-products':
                if($("#checkbox-diapers").is(':checked') || $("#checkbox-products").is(':checked')) {
                    removeMessage(elt);
                    //console.log("checkbox-diapers is checked");
                    if (event == 'check')
                        return true;
                } else {
                    // $(".input__checkbox").addClass("invalid");
                    // $("#checkbox-products").addClass("invalid");
                    // alert("added");
                    //$("#checkbox-diapers, #checkbox-products").css({"border":"1px solid red"});
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01089'))))?>", elt, event);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
            // case 'checkbox-products':
            //     if($(elt).is(':checked')){
            //         removeMessage(elt);
            //         //console.log("checkbox-products is checked");
            //         if (event == 'check')
            //             return true;
            //     } else {
            //         fireMessage("BRO", elt);
            //         //console.log("checkbox-products is NOT checked");
            //         if (event != 'keyup') {
            //             emptyField("<?=getWord('W01119', array(strtolower(getWord('W01089'))))?>", elt, event);
            //             if (event == 'check')
            //                 return false;
            //         }
            //     }
            // break;
            case 'select-size':
                if ($(elt).value != 0) {

                    var size = document.getElementById('select-size').value;
                    if (size == 0) {
                        //console.log("invalid size");
                        fireMessage("Please select a size", document.getElementById('select-size'));
                        //fireMessage("The expiry date is already passed.", elt,20,282);
                        if (event == 'check')
                            return false
                    } else {
                        //removeErrorMessage(elt);
                        removeMessage(document.getElementById('select-size'));
                        if (event == 'check')
                            return true;
                    }
                } else {
                    emptyField("<?=getWord('W01120', array(getWord('W01092')))?>",  document.getElementById('year'));
                    if (event == 'check')
                        return false;
                }
                break;
            case 'gender-boy':
            case 'gender-girl':
                if($("#gender-girl").is(':checked') || $("#gender-boy").is(':checked')) {
                    removeMessage(elt);
                    //console.log("checkbox-products is checked");
                    if (event == 'check')
                        return true;
                } else {
                    //fireMessage("The expiry date is already passed.", elt, 20,120);
                    //console.log("checkbox-products is NOT checked");
                    if (event != 'keyup') {
                        emptyField("<?=getWord('W01119', array(strtolower(getWord('W01089'))))?>", elt, event);
                        if (event == 'check')
                            return false;
                    }
                }
            break;
        }
    }

    /* Displays error message */
    function fireMessage(message, elt){
        var $element = elt.id,
            _this = $('#' + $element),
            emsg = message;

        if(_this.prev('p.msgs')){
            _this.prev('p').remove();
        }

        if(_this.next('small.error')){
            _this.next('small').remove();
        }

        if(_this.next('#green_check')){
            _this.next('#green_check').remove();
        }
        if(_this.hasClass('invalid-empty')){
            _this.removeClass('invalid-empty');
        }

        if(_this.parent().hasClass('error')){
            _this.parent().removeClass('error');
        }
        if(_this.parent().hasClass('valid')){
            _this.parent().removeClass('valid');
        }

        _this.removeClass('valid').addClass('invalid');

        if(_this.attr('id') == 'year') {
            // if($('#month').prev('p.msgs')){
            //     $('#month').prev('p').remove();
            // }

            if(_this.closest('.row').hasClass('error')){
                _this.closest('.row').removeClass('error');
            }

            if($('#month').closest('.row').next('small.error')){
                $('#month').closest('.row').next('small').remove();
            }
            if(typeof brwsIe7 !== 'null' && brwsIe7.length !== 0) {
                // _this.before('<p class="msgs">' + emsg + '</p>');
                _this.after('<small class="error">' + emsg + '</small>');
            } else {
                 // $('#month').before('<p class="msgs">' + emsg + '</p>');
                 $('#month').closest('.row').after('<small class="error">' + emsg + '</small>');
            }
            _this.closest('.row').addClass('error');
            $('#month').removeClass('valid invalid-empty').addClass('invalid');
       } else {
            // _this.before('<p class="msgs">' + emsg + '</p>');
            _this.after('<small class="error">' + emsg + '</small>');
            _this.parent().addClass('error');
       }

    }

    function clearValidation(elt){
        var $element = elt.id;
        var _this = $('#' + $element);

         if(_this.attr('id') == 'year') {
            _this.closest('.row').removeClass('error');
            $('#month').closest('.row').next('small').remove();
         }else {
            _this.next('small').remove();
            _this.removeClass('valid invalid invalid-empty').parent().removeClass('error valid ');
        }

    }
    // Remove Red Box error at top of form.
    function fadeFormError(){
        var emptyError = $("form.empty .empty");
        emptyError.fadeOut(1000);
    }
    /* Removes the error message */
    function removeMessage(elt){

        var $element = elt.id;
        var _this = $('#' + $element);
        var offset = _this.position();
        var width = _this.width();

        // if(_this.prev('p.msgs')){
        //     _this.prev('p').remove();
        // }

        if(_this.next('small.error')){
            _this.next('small').remove();
        }

        if(_this.parent().hasClass('error')){
            _this.parent().removeClass('error');
        }


        fadeFormError(); // Remove red box error once validation of any field.


        //_this.parent().addClass('valid');

        _this.removeClass('empty-field').addClass('valid');
        _this.removeClass('invalid invalid-empty').addClass('valid');

        if(_this.attr('id') == 'year') {
            // if($('#month').prev('p.msgs')){
            //     $('#month').prev('p').remove();
            // }

            if(_this.closest('.row').hasClass('error')){
                _this.closest('.row').removeClass('error');
            }

            if($('#month').closest('.row').next('small.error')){
                $('#month').closest('.row').next('small').remove();
            }

            if($('#month').val() != 0) {
                $('#month').addClass('valid').removeClass('invalid invalid-empty');
            }
        }
    }

    //error message function, sets message and positions relative to input
    function emptyField(message, elt){
        var $element = elt.id,
            _this = $('#' + $element)
            emsg = message;

        // if(_this.next('p.msgs')){
        //     _this.next('p').remove();
        // }

        if(_this.next('small.error')){
            _this.next('small').remove();
        }

        if(_this.parent().hasClass('error')){
            _this.parent().removeClass('error');
        }

        if(_this.next('#green_check')){
            _this.next('#green_check').remove();
        }

        _this.removeClass('valid invalid').addClass('invalid invalid-empty');

       if(_this.is('select#year')){
            if(_this.closest('.row').hasClass('error')){
                _this.closest('.row').removeClass('error');
            }
            if($('#month').closest('.row').next('small.error')){
                $('#month').closest('.row').next('small').remove();
            }
            $('#month').removeClass('valid').addClass('invalid invalid-empty');
       }

    }


    /* Checks if the Credit Card Number is valid, using the Luhn algorithm */
    function luhn_check(number){
        var number=number.replace(/\D/g, '');

        var number_length=number.length;
        var parity=number_length % 2;

        var total=0;
        for (i=0; i < number_length; i++) {
            var digit=number.charAt(i);
            if (i % 2 == parity) {
                digit=digit * 2;
                if (digit > 9)
                    digit=digit - 9;
            }
            total = total + parseInt(digit);
        }

        if (total % 10 == 0)
            return true;
        else
            return false;
    }

    /* Disables the submit button in the form */
    function disableBtn(frm){
        $(frm).find("input[type='submit']").attr("disabled", "disabled");
    }
});
