<?php
    header("Content-type: application/javascript");

    chdir("../../../../");

    require_once("../etc/config/media-network/globals.php");

    Ihub_Http_Helper::cacheComponent(31536000);
    $base_url = getBaseUrl(true);
    $flow_var = "movies/mc/";
    $js_base_url = "js/signup/" . $flow_var;
    $image_url = $base_url . 'images/signup/' . $flow_var;
    $frameworkPath = Ihub_Config::get()->retrieve('front_core','framework')."js/5/";

    ob_start('ob_gzhandler');
?>

/*<script>*/
    <?php
      require($frameworkPath."vendor/jquery.js");
      require($js_base_url."jquery-ui.min.js");
      require($frameworkPath."foundation/foundation.min.js");
      require($frameworkPath."foundation/foundation.reveal.min.js");
       require($frameworkPath."foundation/foundation.interchange.js");
      //require($frameworkPath."foundation/foundation.equalizer.min.js");
      require($js_base_url."scripts.min.js");
      require($js_base_url."validation.min.js");
      require($js_base_url."simple-slider.min.js");
    ?>

    $(document).foundation('reveal', {
      animation: 'fade',
      animation_speed: 150,
      close_on_background_click: false,
      dismiss_modal_class: 'close-reveal-modal',
      bg_class: 'nothing-to-see-here',
      bg : $(''),
    });
/*</script>*/



<?php
    ob_end_flush();
?>
