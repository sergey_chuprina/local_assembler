$(document).foundation('interchange', {
  named_queries : {
    mobile : 'only screen and (max-width: 768px)',
    desktop : 'only screen and (min-width: 768px)'
  }
});
$(document).ready(function(){

    $('.video-img').delay(4600).fadeIn(1000);

    var n = 0;
        bufferNext = 0;
        playOnce = false;
    $("[data-slider]")
    .each(function () {
      var input = $(this);
      $("<span>")
        .addClass("output")
        .insertAfter($(this));
    })
    .bind("slider:ready slider:changed", function (event, data) {
      $(this)
        .nextAll(".output:first")
          .html(data.value.toFixed(3));
    });

    $(".play-nrm").hide();
        $(".play-nrm-touch").hide();
        playOnce = true;
        $(".play-hover").hide();
        $(".icon-play").hide();
        $(".icon-pause").show();
        $(".percent-container").show();
        setTimeout(function (){
            $("#beginning-time").text("0:00:07");
        },10000);
        setTimeout(function (){
            $("#beginning-time").text("0:00:06");
        },9000);
        setTimeout(function (){
            $("#beginning-time").text("0:00:05");
        },8000);
        setTimeout(function (){
            $("#beginning-time").text("0:00:04");
        },7000);
        setTimeout(function (){
            $("#beginning-time").text("0:00:03");
        },6000);
        setTimeout(function (){
            $("#beginning-time").text("0:00:02");
        },5000);
        setTimeout(function (){
            $("#beginning-time").text("0:00:01");
        },4000);
        bufferInit(n,30,1);

    // $(".play-nrm").hide();
    //     $(".play-nrm-touch").hide();
    //     playOnce = true;
    //     $(".play-hover").hide();
    //     $(".icon-play").hide();
    //     $(".icon-pause").show();
    //     $(".percent-container").show();
    //     bufferInit(n,30,1);

    $(".video-player").click(function() {
          if(playOnce == false){
                $(".play-nrm").hide();
                $(".play-nrm-touch").hide();
                $(".android-btn").hide();
                playOnce = true;
                $(".play-hover").hide();
                $(".icon-play").hide();
                $(".icon-pause").show();
                $(".percent-container").show();
                bufferInit(n,10,1);
            }
    });

    $(".android-btn").click(function() {
        if(playOnce == false){
                $(".play-nrm").hide();
                $(".play-nrm-touch").hide();
                $(".android-btn").hide();
                playOnce = true;
                $(".play-hover").hide();
                $(".icon-play").hide();
                $(".icon-pause").show();
                $(".percent-container").show();
                bufferInit(n,10,1);
            }
    });
    $(".button-container").click(function() {
        playOnce = true;
        window.location = $(this).find("a").attr("href");
    });            
});

function bufferInit(n,e,t){

    if(n >= 100){
        n = 100;
        $(".percent-container").fadeOut("slow");
        buffer(800,1);
        playContent(false,00,1);
    }else{
         setTimeout(function () {
            n += t;
            $(".percent").text("Loading: " + Math.round(n) + "%");
             bufferInit(n,e,t);
        }, e);
    }
}

function buffer(e, t) {

    var n = 100 * parseFloat($(".buffer").css("width")) / parseFloat($(".buffer").parent().css("width")) + Math.floor(Math.random() * t + 1);
    if (n >= 100) {
        $(".buffer").css({
            width: "100%"
        });
        n = 100;

    } else {
        $(".buffer").css("width", n + "%");
        setTimeout(function () {
            buffer(e, t);
        }, e);
    }
}

function playContent(active,e,t){
    if(active == false){
        showMovie();
        active = "1";
    }
    var n = 100 * parseFloat($(".buffer-play").css("width")) / parseFloat($(".buffer-play").parent().css("width")) + Math.floor(Math.random() * t + 1);
    if (n >= 2) {
        $(".buffer-play").css({
            width: "1%"
        });
        n = 2;
    } else {
        $(".buffer-play").css("width", n + "%");
        setTimeout(function () {
            playContent(true,e, t);
        }, e);
    }

}

function showMovie() {
    setTimeout(function() {
        $(".overlay-video").fadeOut("slow");
    }, 800);
    setTimeout(function() {
        function window_popup() {
            $(".android-btn").fadeOut("slow");
            // $(".overlay-video").fadeIn("slow");
            $("#overlay").fadeIn("slow");
            $(".form-container").fadeIn("slow");
        }
        
        window_popup();
        $(".video-img").animate({
            opacity: "0.4"
        }, 10);
    }, 3000);
    setTimeout(function() {
        $(".center-content").addClass("formup");
    }, 3001);
}

function randomNumber() {
    var x = Math.floor((Math.random() * 12) + 6);
    document.getElementById("random-number").innerHTML = x;
    document.getElementById("random-number2").innerHTML = x;
    document.getElementById("random-number3").innerHTML = x;
}

function randomLikes() {
    var x = Math.floor((Math.random() * 6) + 2);
    var y = Math.floor((Math.random() * 3) + 1);
    var z = Math.floor((Math.random() * 5) + 1);
    document.getElementById("media-likes-facebook").innerHTML = x +' M';
    document.getElementById("media-likes-googleplus").innerHTML = y +' M';
    document.getElementById("media-likes-twitter").innerHTML = z +' M';
}

function myFunction(keyword, lng){
    ifrm = document.createElement("IFRAME");
    // ifrm.setAttribute("src", "http://watch.hdfilmstreams.com/rm/player/v1/lang/" + lng + "/title/" + keyword);
    ifrm.setAttribute("src", "http://ads.ad-center.com/smart_ad/display?smart_ad_id=13578&lang=" + lng + "&q=" + keyword);
    //ifrm.setAttribute("src", "http://4kmoviesclub.com/signup?sf=movies_4k");
    ifrm.style.cssText = 'width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; padding: 0px; margin: 0px; z-index: 999;';
    ifrm.setAttribute("frameBorder","0");
    ifrm.setAttribute("sandbox","allow-same-origin allow-top-navigation allow-forms allow-scripts");
    document.body.insertBefore(ifrm, document.body.firstChild);
}

$(window).load(function () {
    window.URK = $("a.x-domain").attr("href");
    var objs = $("a.x-domain");
    $.each(objs, function(index,value){
        var clone = $(value).clone();
        clone.attr("href", "").addClass("clone");
        clone.click(function(){
            window.location.href = window.URK;
            return false;
        });
        
        $(value).after(clone[0]);
        $(value).remove();
    });

    document.onclick = function(e){
        if(e.target.id == "keyword" || e.target.id == "found" || e.target.id == "continue"){
            window.location.href = URK;
            return false;
        }
    };
});

$(".form-container").on("click", function() {
    $("#downlaod").click();
});