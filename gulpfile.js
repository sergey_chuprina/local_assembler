'use strict';

var path = {
		src: 'movies/mc',
		dist: 'movies/md',
		netDisk: 'S:/step0/',

		htdocs: 'htdocs/',
		media: 'app-media-network/'
	},
	lang = 'EN'; // 'EN', 'FR', 'IT', 'ES', 'DE', 'PT', 'IN', 'BE', 'DK', 'SE', 'NO', 'JP', 'FI', 'NL', 'ZH_CN', 'ZH_TW'




	

var fs = require("fs"),
fse = require('fs-extra'),
gulp = require('gulp'),
$ = require('gulp-load-plugins')({lazy: true}),
runSequence = require('run-sequence'),
linksReplace = require('./linksReplace').links,
browserSync = require('browser-sync').create();

var translator = path.netDisk + path.media + 'managers/word_translator_manager.php',
foundationScssPath = path.netDisk + 'vendor/jomedia/frontcore/foundation/scss/',
mainStyleSheet = '_style.scss',
distPath = {
	step0: path.htdocs + 'views/signup/' + path.dist,
	layouts: path.htdocs + 'views/layouts/signup/' + path.dist,
	scss: path.htdocs + 'scss/signup/' + path.dist,
	css: path.htdocs + 'css/signup/' + path.dist,
	js: path.htdocs + 'js/signup/' + path.dist,
	imgs: path.htdocs + 'images/signup/' + path.dist,
	html: path.htdocs + 'html/signup/' + path.dist,
	frontcore: path.htdocs + 'frontcore/',
	output: path.htdocs + 'output/signup/' + path.dist
},
frontcore = {
	path : [
	path.netDisk + 'vendor/jomedia/frontcore/foundation/js/5/vendor/jquery.js',
	path.netDisk + 'vendor/jomedia/frontcore/foundation/js/5/foundation/foundation.min.js',
	path.netDisk + 'vendor/jomedia/frontcore/foundation/js/5/foundation/foundation.reveal.min.js',
	path.netDisk + 'vendor/jomedia/frontcore/foundation/js/5/foundation/foundation.interchange.js'
	],
	src :
	'\n<script src="frontcore/jquery.js"></script>' +
	'\n<script src="frontcore/jquery-ui.min.js"></script>' +
	'\n<script src="frontcore/foundation.min.js"></script>' +
	'\n<script src="frontcore/foundation.reveal.min.js"></script>' +
	'\n<script src="frontcore/foundation.interchange.js"></script>\n'

},
localSrcPath = {
	step0: path.htdocs + 'views/signup/' + path.src,
	layouts: path.htdocs + 'views/layouts/signup/' + path.src,
	scss: path.htdocs + 'scss/signup/' + path.src,
	css: path.htdocs + 'css/signup/' + path.src,
	js: path.htdocs + 'js/signup/' + path.src,
	imgs: path.htdocs + 'images/signup/' + path.src,
	html: path.htdocs + 'html/signup/' + path.src,
};


//========================================================================================

gulp.task('copy:content:dev', () => {
	var allIn = '/**/*.*';
	return gulp.src([
		path.netDisk + path.htdocs + 'js/signup/' + path.src + allIn,
		path.netDisk + path.htdocs + 'images/signup/' + path.src + allIn
		])
	.pipe(gulp.dest((f) => {
		return (f.path.indexOf('\\js\\signup\\') >= 0) ? distPath.js + '/' :
		(f.path.indexOf('\\images\\signup\\') >= 0) ? distPath.imgs + '/' :
		null;
	})).pipe($.if((f) => {
		return f.path.indexOf('scripts.js') >= 0;
	}, $.open()));
});

gulp.task('copy:scss:dev', ['copy:content:dev'], () => {
	fse.mkdirpSync(distPath.scss);
	const stylesheetsPath = path.netDisk + path.htdocs + 'scss/signup/' + path.src;
	var format = "utf8";

	var styleFiles = fs.readdirSync(stylesheetsPath, format);
	styleFiles.forEach((el) => {
		var fileName = el;
		el = stylesheetsPath + '/' + el;
		var styles = fs.readFileSync(el, format);
		var regexp = /<\?=\s?\$image_url\s?\?>/;
		while(styles.search(regexp) >= 0){
			styles = styles.replace(regexp, '../images');
		}
		el = distPath.scss + '/' + fileName;
		fs.writeFile(el, styles);
	});
	return gulp.src(distPath.scss + '/' + mainStyleSheet)
	.pipe($.open());
});

gulp.task('copy:frontcore:dev', ['copy:scss:dev'], () => {
	var p = fs.readdirSync(path.htdocs);
	if(p.indexOf('frontcore') >= 0) return;
	return gulp.src(frontcore.path)
	.pipe(gulp.dest(distPath.frontcore));
});

gulp.task('tpl:dev', ['copy:frontcore:dev'], () => {
	fse.mkdirpSync(distPath.html);
	const contentPath = [
	path.netDisk + path.media + 'views/layouts/signup/' + path.src + '/header.tpl',
	path.netDisk + path.media + 'views/signup/' + path.src + '/step0.tpl',
	path.netDisk + path.media + 'views/layouts/signup/' + path.src + '/footer.tpl'
	];
	var pageContent = '';
	var format = "utf8";
	contentPath.forEach((el, i, arr) => {
		let content = fs.readFileSync(el, format);
		while(content.indexOf('<?=$base_url?>js/signup<?=$flow_var?>/main.php?cache_control') >= 0){
			var start = content.indexOf('<?=$base_url?>js/signup<?=$flow_var?>/main.php?cache_control');
			var end = content.indexOf(')?>', start) + 3;
			if(end >= 0){
				var oldSrc = content.substring(start,end);
				var newSrc = 'js/scripts.min.js';
				content = content.replace(oldSrc, newSrc);
			}
		}
		if(i === 2){
			var scripts = '';
			fs.readdirSync(distPath.js, format).forEach((el) => {
				if(el.indexOf('min.js') >= 0){
					if(el.indexOf('scripts.min.js') >= 0 || el.indexOf('jquery-ui.min.js') >= 0 || el.indexOf('modernizr') >= 0) return;
					scripts += '\n<script src="js/' + el + '"></script>\n';
				}
			});
			content = scripts + content;
			content = frontcore.src + content;
		}
		linksReplace.forEach((el) => {
			while(content.indexOf(el.old) >= 0){
				content = content.replace(el.old, el.new);
			}
		});
		pageContent += content;
	});
	pageContent += '\n</html>';

	var translatorContent = fs.readFileSync(translator, format);
	var getWord = /getWord\(['"]\w\d{5}['"]\)/i;
	var regFrase = /['"].+[^=>]['"][,;]/gi;
	while(getWord.exec(pageContent)){
		pageContent = pageContent.replace(getWord, () => {
			var arrayNumber = pageContent.match(getWord)[0].match(/\w\d{5}/i)[0];
			var wordsArrayStart = translatorContent.search(arrayNumber);
			var langStart = translatorContent.indexOf(lang, wordsArrayStart) + lang.length + 1;
			regFrase.lastIndex = langStart;
			var frase = regFrase.exec(translatorContent)[0];
			return frase.slice(0, frase.length - 1);
		});
	}
	var varInPHP = /\s?\$\w{1,2}\d{4}\s?=/gi;
	var varInHTML = /<\?=\s?\$\w{1,2}\d{1,4}\s?\?>/gi;
	var obj = {};
	pageContent.match(varInPHP).forEach((el)=>{
		regFrase.lastIndex = pageContent.indexOf(el);
		var frase = regFrase.exec(pageContent)[0];
		frase = frase.slice(1,frase.length - 2);
		var screen = /\\'/;
		frase = frase.replace(screen, "'");
		el = '<?=' + el.match(/\$\w{1,2}\d{4}/gi)[0] + '?>';
		obj[el] = frase;
	});
	pageContent.match(varInHTML).forEach((htmlvar)=>{
		htmlvar = '<?=' + htmlvar.match(/\$\w{1,2}\d{4}/gi)[0] + '?>';
		for(var phpvar in obj){
			if(htmlvar === phpvar){
				pageContent = pageContent.replace(htmlvar, obj[phpvar]);
			}
		}
	})

	fs.writeFile(distPath.html + "/index.tpl", pageContent);
	return gulp.src(distPath.html + "/index.tpl")
	.pipe($.open());
});

gulp.task('copy:dev:start', ['tpl:dev']);

gulp.task('copy:dev', () => {
	return runSequence('copy:dev:start', 'default');
});


//========================================================================================


gulp.task('copy:local', () => {
	var allIn = '/**/*.*';
	return gulp.src([
		localSrcPath.html + allIn,
		localSrcPath.js + allIn,
		localSrcPath.scss + allIn,
		localSrcPath.img + allIn,
		])
	.pipe(gulp.dest((f) =>{
		return (f.path.indexOf('\\scss\\signup\\') >= 0) ? distPath.scss + '/' :
		(f.path.indexOf('\\js\\signup\\') >= 0) ? distPath.js + '/' :
		(f.path.indexOf('\\images\\signup\\') >= 0) ? distPath.imgs + '/' :
		(f.path.indexOf('\\html\\signup\\') >= 0) ? distPath.html + '/' :
		null;
	})).pipe($.if((f) =>{
		return f.path.indexOf('index.tpl') >= 0 ||
		f.path.indexOf('scripts.js') >= 0 ||
		f.path.indexOf(mainStyleSheet) >= 0;
	}, $.open()));
});


//========================================================================================


// =======================================#JS====================================

gulp.task('translate:js', () => {
	var contentPath = distPath.js + '/scripts.js';
	var format = "utf8";
	var translatorContent = fs.readFileSync(translator, format);
	var pageContent = fs.readFileSync(contentPath, format);
	var getWord = /<\?=\s?getWord\(['"]\w\d{5}['"]\)\s?\?>/i;
	var regFrase = /['"].+[^=>]['"][,;]/gi;
	while(getWord.exec(pageContent)){
		pageContent = pageContent.replace(getWord, () => {
			var arrayNumber = pageContent.match(getWord)[0].match(/\w\d{5}/i)[0];
			var wordsArrayStart = translatorContent.search(arrayNumber);
			var langStart = translatorContent.indexOf(lang, wordsArrayStart) + lang.length + 1;
			regFrase.lastIndex = langStart;
			var frase = regFrase.exec(translatorContent)[0];
			frase = frase.slice(1, frase.length - 2);
			var screen = /\\'/;
			return frase = frase.replace(screen, "'");
		});
	}
	fs.writeFile(distPath.js + "/scripts.js", pageContent);
});

gulp.task('js', ['translate:js'],() => {
	return gulp.src(distPath.js + '/scripts.js')
	.pipe($.uglify().on( 'error', $.notify.onError({title  : "JS Error!"})))
	.pipe($.rename({suffix: '.min'}))
	.pipe(gulp.dest(distPath.output + '/js/'))
	.pipe(browserSync.stream());
});

gulp.task('sass', () =>{
	return gulp.src(distPath.scss + '/' + '*.scss')
	.pipe($.sass({includePaths: [foundationScssPath]}).on( 'error', $.notify.onError({title : "Sass Error!"})))
	.pipe($.concat('app.css'))
	.pipe($.autoprefixer(['last 30 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
	.pipe($.csscomb())
	.pipe($.cssnano())
	.pipe(gulp.dest(distPath.output + '/css/'))
	.pipe($.notify({title : "Sass success!"}))
	.pipe(browserSync.stream());
});

gulp.task('img', () => {
	return gulp.src(distPath.imgs + '/*.*')
	.pipe(gulp.dest(distPath.output + '/images/'))
	.pipe(browserSync.stream());
});

gulp.task('html', () => {
	return gulp.src(distPath.html + '/index.tpl')
	.pipe($.rename({extname: ".html"}))
	.pipe(gulp.dest(distPath.output + '/'));
});

gulp.task('frontcore', () => {
	return gulp.src([distPath.frontcore + '/**/*.*', distPath.js + '/jquery-ui.min.js'])
	.pipe(gulp.dest(distPath.output + '/frontcore/'))
	.pipe(browserSync.stream());
});

// #watch
gulp.task('watch', () => {
	gulp.watch([distPath.scss + '/' + '*.scss'], ['sass']);
	gulp.watch(distPath.js + '/scripts.js', ['js']);
	gulp.watch(distPath.html + '/index.tpl', ['html']).on('change', browserSync.reload);
	gulp.watch(distPath.imgs + '/*.*', ['img']);
	gulp.watch(distPath.frontcore + '/*.*', ['frontcore']);
});

gulp.task('serve', ['js', 'sass', 'img', 'html', 'frontcore'], () =>{
	startBrowserSync(distPath.output);
});

function startBrowserSync(basePaths) {
	let options = {
		port: 5000,
		ghostMode: {
			clicks: false,
			location: false,
			forms: false,
			scroll: true
		},
		injectChanges: true,
		logFileChanges: true,
		logLevel: 'debug',
		logPrefix: 'gulp-patterns',
		notify: true,
		reloadDelay: 0,
		online: false,
		server: {
			baseDir: basePaths
		},
		open: true
	};
	browserSync.init(options);
}

gulp.task('default', ['serve','watch']);

//===============================================================================================